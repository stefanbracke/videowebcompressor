require 'yaml'

class Preferences
    
    attr_accessor :data
    
    def initialize
	
	if RUBY_PLATFORM =~ /darwin/
		prefspath = "/Library/Application Support/VideoWebCompressor/prefs.yaml"
	elsif RUBY_PLATFORM =~ /linux/
		prefspath = "~/.config/com.gigamedium.videowebcompressor/prefs.yaml"
	else
		prefspath = "prefs.yaml"
	end


        @data = begin
            YAML.load(File.open(prefspath))
        rescue ArgumentError => e
            puts "Could not parse YAML: #{e.message}"
        end
    end
end
