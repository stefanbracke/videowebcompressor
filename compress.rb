require_relative 'uploader'
require_relative 'emailer'

require 'faraday'

class Compress
    def initialize prefs      
        @prefs = prefs        
        @folder_output = @prefs.data["path_output"]
        @folder_input = @prefs.data["path_input"]
        @path_ffmpeg_ogg = @prefs.data["path_ffmpeg_ogg"]
        @path_ffmpeg_mp4 =@prefs.data["path_ffmpeg_mp4"]
        
        @logger = false
    end
    
    def setlogger (logobject)
        @logger = logobject
    end
        
    def process(videoid, videopath, videourl, videoformatmp4, videoformatogg, videoformatwebm,  videosource, forceupdate)
        @videoid = videoid
        @videopath = videopath
        @videourl = videourl
        @videosource = videosource
        @videoformatogg = videoformatogg
        @videoformatwebm = videoformatwebm
        @videoformatmp4 = videoformatmp4
        @videoextensionorg = File.extname(@videopath)
        @videobasefile = File.basename(@videopath)
        @forceupdate = forceupdate
        @logger.info "COMPRESS: Process item #{@videoid} = #{@videobasefile} mp4:#{@videoformatmp4.to_s}  ogg:#{@videoformatogg.to_s}"
        
        @mailmsg = ""
        
        # Always delete local files if forced!
        if @forceupdate == "1"
            @mailmsg << "Force compression for file #{@videoid} \n"
            path_file_storage = "#{@folder_input}/#{@videobasefile}"
            if File.exists?(path_file_storage)
               begin
                   @logger.info "Compress: delete local file #{path_file_storage}"
                   @mailmsg << "Destroyed local file #{@path_file_storage} \n"
                   File.delete(path_file_storage) 
               rescue
                   @logger.warning "Could not delete local file #{path_file_storage}"
               end
            end
        end
        
        if !remote_file_exist?(".mp4") or @forceupdate == "1"
            compress("mp4", ".mp4")
        end
        
        if !remote_file_exist?(".ogg") or @forceupdate == "1"
            compress("ogg", ".ogg")
        end
        
        if @mailmsg != ""
            mailer = Emailer.new(@prefs)
            mailer.send_email @prefs.data["mailer_to"], :body => @mailmsg
        end
    end
    
    def remote_file_exist?(extension)
        fullurl = "#{@videourl}#{@videoid}#{extension}"
        
        if Faraday.head(fullurl).status == 200
            true
        else
            false
        end
    end
            
    def compress(type,newextension)
        path_file_storage = "#{@folder_input}/#{@videobasefile}"
        path_file_output = "#{@folder_output}/#{@videoid}#{newextension}"
        #puts "COMPRESS: compress #{type} for #{@videoid} store:#{path_file_storage} output:#{path_file_output}"
        
        fileisnew = false
        fileislocal = false
        
        if File.exists?(path_file_storage)
           fileislocal = true
        else 
            #check if source url exists and if not download it locally
            @logger.info  "COMPRESS: downloading to local:#{path_file_storage} using 'curl -sS -o #{path_file_storage} #{@videosource}'"
            output = `curl -sS -o #{path_file_storage} #{@videosource}`
            
            if output
                if File.exists?(path_file_storage)
                    fileislocal = true
                    fileisnew = true
                else
                    @logger.warn  "COMPRESS: error downloading file: #{@videosource} #{output}"
                    fileislocal = false
                end
               
            else 
                # No errors whatsover
                fileislocal = true
                fileisnew = true
            end
        end
        
        if fileislocal
            # get aspect ratio of local file
            ffmpeginfo = `'#{@path_ffmpeg}' -i #{path_file_storage} 2>&1`
            aspectratiosearch = /DAR\s*(\d+:\d+)/.match(ffmpeginfo)
            
            if aspectratiosearch != nil then
                aspectratio = aspectratiosearch[1]
                @logger.info  "COMPRESS: aspect ratio found: #{aspectratio}"
            else
                aspectratio = "5:4"
            end
            
            compressfile = true
            uploadfile = true
            
            if File.exists?(path_file_output)  
                #always erase local compressed file if a new source download is made
                if @forceupdate == "1" 
                    @logger.info  "COMPRESS: output exists and erased: #{path_file_output}"
                    File.delete(path_file_output)
                else
                    @logger.info  "COMPRESS: output exists (no need to compress): #{path_file_output}"
                    compressfile = false
                end
            end
            
            if compressfile
                compress_item(type, path_file_storage, path_file_output, aspectratio)
                
                if File.exists?(path_file_output)
                    @mailmsg << "Compressed into #{type} \n"
                    uploadfile = true
                else
                    uploadfile = false
                    @logger.warn "COMPRESS: ERROR compressed file does not exists: #{path_file_output}"
                end
            end
            
            if uploadfile 
                uploader = Uploader.new(@prefs)
                uploader.setlogger(@logger)
                uploader.uploadFile(path_file_output)
                @mailmsg << "Uploaded file #{@videoid}#{newextension} \n"
            end
        end 
    end

    def compress_item (type, path_file_storage, path_file_output, aspectratio)
        
        @logger.info "Compress: #{path_file_output} with aspectratio:#{aspectratio}"
        compresssettings = ""
        
        bitrate = "1500"  #6144
        videosize = "720x576"
        #videosize = ""; #forget for just now
        
        case type
                    
        when "mp4"
            compresssettings = @prefs.data["compressor_mp4"]
            compresscommand = "'#{@path_ffmpeg_mp4}' -i '#{path_file_storage}' #{compresssettings} -s #{videosize} -aspect #{aspectratio}  \'#{path_file_output}\'"
            #puts "#{compresscommand}"
            `#{compresscommand}`
        when "ogg"
            compresssettings = @prefs.data["compressor_ogg"]
            compresscommand = "'#{@path_ffmpeg_ogg}' -i '#{path_file_storage}' #{compresssettings} -s #{videosize} -aspect #{aspectratio} \'#{path_file_output}\'"
            #puts "COMPRESS ogg:#{compresscommand}"
            `#{compresscommand}`
        when "webm"
            compresssettings = '-c:a libvorbis -ab 128k -aq 4 -ar 22050 -ac 2 -strict experimental -c:v libvpx -b 1612k -bt 224k -minrate 1612k -maxrate 3224k -bufsize 531.96k -r 15 -s 640x480 -aspect 768:576 -me_range 16 -i_qfactor 0.71 -b_strategy 1 -qmin 3 -qmax 51 -qdiff 4 -sc_threshold 40 -sn -threads 24 -nostats -y '
            compresscommand = "'#{@path_ffmpeg}' -i '#{path_file_storage}' #{compresssettings} '#{path_file_output}'"
            #NSLog("{self} '#{@path_ffmpeg}' #{debuglevel} -i '#{path_file_storage}' #{compresssettings} \'#{path_file_output}\'")
            `#{compresscommand}`
        else
            @logger.warn  "unknown type to compress"
        end
    end
end