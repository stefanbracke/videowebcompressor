VideoWebCompressor

Author: Stefan Bracke
Date: 2 June 2013

Description:
- Parses an online XML document
- Every video required will be
	- Downloaded to a source folder (if not already or if forced)
	- Compressed into the required format(s)
	- Upload the result back to the server via ftp

Preferences:
The application will look for them in /Library/Application Support/VideoWebCompressor/prefs.yaml
See the example file which explains the required settings

Gems:
- use bundler to automatically load the required rubygemsr
