require 'net/smtp'
require 'mail'

class Emailer
    
    def initialize prefs
        @prefs = prefs
    end
    
    def send_email(sendto,opts={})
        opts[:from]        ||= @prefs.data["mailer_from"]
        opts[:from_alias]  ||= 'VideoWebCompressor'
        opts[:subject]     ||= @prefs.data["mailer_subject"]
        opts[:body]        ||= "Message of email"
                
        options = { 
            :address              => @prefs.data["mailer_smtp"],
            :port                 => @prefs.data["mailer_port"],
            :domain               => @prefs.data["mailer_domain"],
            :user_name            => @prefs.data["mailer_user_name"],
            :password             => @prefs.data["mailer_password"],
            :authentication       => @prefs.data["mailer_authentication"],
            :enable_starttls_auto => true  
        }
        
        Mail.defaults do
            delivery_method :smtp, options
        end
        
        Mail.deliver do
            to  sendto
            from opts[:from]
            subject opts[:subject] 
            body opts[:body]
        end
    end
end