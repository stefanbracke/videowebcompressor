#!/usr/bin/env  /usr/local/Cellar/ruby/2.0.0-p195/bin/ruby
# 
# VideoWebCompressor
#
# Automatically compress videos
#
# Author: Stefan Bracke <stef@digicombo.eu>
# Date: 2 June 2013

require_relative 'prefs'
require_relative 'compress'
require 'net/http'
require 'rexml/document'
require 'logger'
require 'rubygems'
require 'bundler/setup'

def main
    @prefs = Preferences.new
    
    @logger = Logger.new(@prefs.data["path_log"], 10, 1024000); # 1MB log max, max 10 logs
    @logger.datetime_format = "%Y-%m-%d %H:%M:%S"
    @logger.info "VideoWebCompressor started"

    data = Downloader(@prefs.data["url_source_xml"])
    XMLParser(data)
    @logger.close 
end

def Downloader urltoparse
	uri = URI.parse(urltoparse)

	begin
   		response = Net::HTTP.get_response(uri);
   		if response.code != "200"
           raise
   		end
        return response.body
	rescue
        @logger.fatal "VideoWebCompressor could not parse #{urltoparse}"
   		puts "ERROR: cannot download #{uri}"
   	    exit 1
    end
end

def XMLParser data
    xmldoc = REXML::Document.new data
    xmldoc.elements.each('videos/file') { |fileitem|
        videoid = fileitem.attributes['id']
        videopath =  fileitem.attributes['path']
        videourl = fileitem.attributes['videourl']  #GENERIC place to put videos 
        videoformatmp4 = fileitem.attributes['format_mp4']
        videoformatogg = fileitem.attributes['format_ogg']
        videoformatwebm = fileitem.attributes['format_webm']
        forceupdate = fileitem.attributes['forceupdate']
        videosource = fileitem.text
        
        compressor = Compress.new(@prefs)
        compressor.setlogger(@logger)
        compressor.process(videoid, videopath, videourl, videoformatmp4, videoformatogg, videoformatwebm,  videosource,forceupdate)
    }
end

main()
