require 'net/ftp'

class Uploader

  def initialize prefs
      @protocol = "ftp" 
      @prefs = prefs
    end
    
    def setlogger logObject
        @logger = logObject
    end
    
    def uploadFile (filepath)
        @logger.info "FTP: uploadfile (#{@protocol}) #{filepath} -> server:#{@prefs.data['ftp_server']} dir:#{@prefs.data['ftp_dir']}"
        
        case @protocol
            when "ftp"
                begin
                    ftp=Net::FTP.new
                    ftp.connect(@prefs.data['ftp_server'],@prefs.data['ftp_port'])
                    ftp.login(@prefs.data['ftp_user'],@prefs.data['ftp_pass'])
                    ftp.chdir(@prefs.data['ftp_dir'])
                    ftp.putbinaryfile(filepath)
                    ftp.close
                    @logger.info "FTP: Upload done #{filepath} "
               rescue
                    @logger.warn "FTP: Error uploading using ftp"
                end
            else
                puts "FTP: Unknown protocol #{@procotol}"
        end
    end
end

